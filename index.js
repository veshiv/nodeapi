const express=require("express");
const mysql=require("mysql");
//import  db from './config.db.js';
//db config 
//require("./config/db.js");

const app=express();

var db =mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'root',
    database:'angularblog'
})

db.connect((err)=>{
    if(err){
        throw err;
    }
    console.log('Mysql connected....');
});

app.get('/',(req,res)=>{
    res.send("Working......")
});

// Select posts
app.get('/api/posts', (req, res) => {
    let sql = 'SELECT blog_title,blog_text,blog_image from blogs INNER JOIN blog_details on blogs.id=blog_details.blog_id';
    let query = db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.send(JSON.stringify(results));
    });
});
// Select posts
app.get('/api/post/:id', (req, res) => {
    let sql = `SELECT blog_title,blog_text,blog_image from blogs INNER JOIN blog_details on blogs.id=blog_details.blog_id where blogs.id=${req.params.id}`;
    let query = db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.send(JSON.stringify(results));
    });
});

app.listen('3000',()=>{
    console.log("Server started on port 3000")
})
