
const mysql =require('mysql');

var db = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'root',
    database:'angularblog'
})

db.connect((err)=>{
    if(err){
        throw err;
    }
    console.log('Mysql connected....');
});

export db;
